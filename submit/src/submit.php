<?php
    require_once('config.php');
    require_once('header.php');

    if(isset($_POST["prijavi"])){
      $nazivP = $_POST['predmeti'];
      $rokN = $_POST['rokovi'];

      $brindsql = "SELECT `br_ind` FROM `studenti`, `korisnici` WHERE studenti.email=korisnici.email AND uname = '$name';";
      $jmbgsql = "SELECT `jmbg` FROM `studenti`, `korisnici` WHERE studenti.email=korisnici.email AND uname = '$name';";
      $smersql = "SELECT `smer_id` FROM `studenti`, `korisnici`, `smer`
                  WHERE `uname` = '$name'
                  AND studenti.smer = naziv_smera";
      $predmetsql = "SELECT `predmet_id` FROM  `predmet`
                     WHERE naziv_predmeta = '$nazivP'";
      $roksql = "SELECT `rok_id` FROM `rokovi` WHERE `rok_naziv` = '$rokN'";

      $qbrind = $conn -> query($brindsql);
      $fetch = $qbrind->fetchALL(PDO::FETCH_OBJ);
      foreach ($fetch as $f) {
        $brind = $f->br_ind;
      }

      $qjmbg = $conn-> query($jmbgsql);
      $fetch = $qjmbg->fetchALL(PDO::FETCH_OBJ);
      foreach ($fetch as $f) {
        $jmbg = $f->jmbg;
      }

      $qsmer = $conn-> query($smersql);
      $fetch = $qsmer->fetchALL(PDO::FETCH_OBJ);
      foreach ($fetch as $f) {
        $smer = $f->smer_id;
      }

      $qp = $conn-> query($predmetsql);
      $fetch = $qp->fetchALL(PDO::FETCH_OBJ);
      foreach ($fetch as $f) {
        $predmet_id = $f->predmet_id;

      }
      $qrok = $conn-> query($roksql);
      $fetch = $qrok->fetchALL(PDO::FETCH_OBJ);
      foreach ($fetch as $f) {
        $rok_id = $f->rok_id;
      }

            $ispitInsert = "
                    INSERT INTO `ispit`
                    SET `br_ind` = :br_ind,
                        `jmbg` = :jmbg,
                        `smer_id` = :smer,
                        `predmet_id` = :predmet,
                        `naziv_predmeta` = :naziv_predmeta,
                        `rok` = :rok
                    ";
            $ispi = $conn -> prepare($ispitInsert);
            $ispi -> execute(array(
                                ':br_ind' => $brind,
                                ':jmbg' => $jmbg,
                                ':smer' => $smer,
                                ':predmet' => $predmet_id,
                                ':naziv_predmeta' => $nazivP,
                                ':rok' => $rok_id
                                ));
  
    }
?>
<form name="prijavi" action=prijavi.php method="post">
				<label for="predmet"><b>Pedmet: </b></label>
                <br>
        <select name='predmeti'>
          <?php
          $sqlPredmet = "SELECT `naziv_predmeta` FROM `predmet`";

          foreach ($conn->query($sqlPredmet) as $row)
          {
            echo "<option name='$row[naziv_predmeta]' value='$row[naziv_predmeta]' >$row[naziv_predmeta]</option>";
          }

          ?>
        </select>
				<br>
				<label for="rok"><b>Ispitni rok: </b></label>
        <br>
        <select name='rokovi'>
          <?php
          $sqlRok = "SELECT `rok_naziv` FROM `rokovi`";

          foreach ($conn->query($sqlRok) as $row)
                {
                  echo "<option name='$row[rok_naziv]' value='$row[rok_naziv]' >$row[rok_naziv]</option>";
                }
                ?>
        	  </select><br>
				 <input type="submit" name="prijavi" value="prijavi"></input>
			</form>

<?php if(!isset($_SESSION['uname'])){
  header("location:index.php");
}
include_once('footer.php');
?>
