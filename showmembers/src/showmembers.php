<?php
  include_once('config.php');
  include_once('header.php');

  $sql = "SELECT * FROM `korisnici`";
  try{
    $korisnici = $conn -> query($sql);
    $fetch = $korisnici->fetchALL(PDO::FETCH_OBJ);
  }catch(PDOexception $e){
      echo $e -> getMessage();
      die();
  }

?>

  <table>
      <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Mail</th>
        <th>Datum registracije</th>
      </tr>
        <?php
          foreach ($fetch as $f) {
        ?>
          <tr>
            <td>
              <?php echo $f->id;?>
            </td>
            <td>
              <?php echo $f->uname;?>
            </td>
            <td>
              <?php echo $f->email;?>
            </td>
            <td>
              <?php echo $f->dat_reg;?>
            </td>
          </tr>
        <?php
          }

if($name <> 'admin') {header("Location: index.php");}
 include_once('footer.php');
