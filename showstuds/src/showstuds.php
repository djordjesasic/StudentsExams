<?php
  include_once('config.php');
  include_once('header.php');

  $sql = "SELECT * FROM `studenti`";
  try{
    $korisnici = $conn -> query($sql);
    $fetch = $korisnici->fetchALL(PDO::FETCH_OBJ);
  }catch(PDOexception $e){
      echo $e -> getMessage();
      die();
  }

?>

  <table>
      <tr>
        <th>ID</th>
        <th>Broj indeksa</th>
        <th>jmbg</th>
        <th>Ime</th>
        <th>Prezime</th>
        <th>email</th>
        <th>smer</th>
      </tr>
        <?php
          foreach ($fetch as $f) {
        ?>
          <tr>
            <td>
              <?php echo $f->student_id;?>
            </td>
            <td>
              <?php echo $f->br_ind;?>
            </td>
            <td>
              <?php echo $f->jmbg;?>
            </td>
            <td>
              <?php echo $f->ime;?>
            </td>
            <td>
              <?php echo $f->prezime;?>
            </td>
            <td>
              <?php echo $f->email;?>
            </td>
            <td>
              <?php echo $f->smer;?>
            </td>
          </tr>
        <?php
          }

if($name <> 'admin') {header("Location: index.php");}
 include_once('footer.php');
