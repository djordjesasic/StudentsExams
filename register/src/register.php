<?php
    require_once('config.php');
    require_once('header.php');
    $err="";

    if(isset($_POST["registruj"])){
        $usrcheck = "SELECT * FROM `korisnici` WHERE `uname` = :uname";
        $users = $conn->prepare($usrcheck);
        $users -> execute(array(
                                ':uname' => $_POST['uname']
                                ));
        if($users->rowCount()){
            $err .= "Korisnicko ime vec postoji.";
        }else{
            $uname = $_POST['uname'];
        }

        $mailcheck = "SELECT * FROM `korisnici` WHERE `email` = :email";
        $mails = $conn->prepare($mailcheck);
        $mails -> execute(array(
                                ':email' => $_POST['email']
                                ));
        if($mails->rowCount()){
            $err .= "email vec postoji.";
        }else{
            $email = $_POST['email'];
        }

        if ($_POST['pass'] != $_POST['repass']){
            $err .= "Lozinke se ne podudaraju";
        }else{
            $pass = openssl_digest($_POST['pass'], 'sha512') ;
        }
        $ime = $_POST['ime'];
        $prezime = $_POST['prezime'];
        $brind = $_POST['brind'];
        $jmbg = $_POST['jmbg'];
        $smer = $_POST['smer'];
        $date = date('d-m-y H:i:s');
        $datereg = (string)$date;

        if($err <> ""){
            echo "<script type='text/javascript'>alert('$err');</script>";
        }else{
            $stdInsert = "
                    INSERT INTO `studenti`
                    SET `br_ind` = :br_ind,
                        `jmbg` = :jmbg,
                        `ime` = :ime,
                        `prezime` = :prezime,
                        `email` = :email,
                        `smer` = :smer
                    ";
            $stdi = $conn -> prepare($stdInsert);
            $stdi -> execute(array(
                                ':br_ind' => $brind,
                                ':jmbg' => $jmbg,
                                ':ime' => $ime,
                                ':prezime' => $prezime,
                                ':email' => $email,
                                ':smer' => $smer
                                ));
            $usrInsert = "
                    INSERT INTO `korisnici`
                    SET `uname` = :uname,
                        `email` = :email,
                        `pass` = :pass,
                        `dat_reg` = :datreg
                        ";
            $usri = $conn -> prepare($usrInsert);
            $usri -> execute(array(
                                ':uname' => $uname,
                                ':email' => $email,
                                ':pass' => $pass,
                                ':datreg' => $datereg
                                ));
            echo "<br>" . "Uspesno ste registrovani." . "<br>" . "<a href='login.php'>uloguj se</a>" ;
        }
    }
?>
    <form name="register" action=register.php method="post">
				<label for="uname"><b>Username</b></label>
                <br>
				<input type="text" placeholder="Enter Username" name="uname" required>
				<br>
				<label for="sifra"><b>Sifra</b></label>
                <br>
				 <input type="password" name="pass" required>
				 <br>
                 <label for="sifra"><b>Sifra ponovo</b></label>
                 <br>
				 <input type="password" name="repass" required>
				 <br>
				 <label for="email"><b>email</b></label>
                 <br>
				 <input type="text" name="email" required>
				 <br>
				 <label for="ime"><b>ime</b></label>
                 <br>
				 <input type="text" name="ime" required>
				 <br>
				 <label for="prezime"><b>prezime</b></label>
                 <br>
				 <input type="text" name="prezime" required>
				 <br>
				 <label for="brind"><b>Broj indeksa</b></label>
                 <br>
				 <input type="text" name="brind" required>
				 <br>
				 <label for="jmbg"><b>JMBG</b></label>
                 <br>
				 <input type="text" name="jmbg" required>
				 <br>
				 <label for="smer"><b>Smer</b></label>
                 <br>
				 <input type="text" name="smer" required>
				 <br>
				 <input type="submit" name="registruj" value="Registruj se"></input>
			</form>
<?php include_once('footer.php');
if(isset($_SESSION['uname'])){
  header("location:index.php");
} ?>