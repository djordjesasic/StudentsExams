<?php
    include_once('config.php');
    include_once('header.php');

    $sql = "SELECT studenti.ime, studenti.prezime, studenti.br_ind, studenti.jmbg, studenti.smer, ispit.naziv_predmeta, rokovi.rok_naziv
    FROM studenti 
    INNER JOIN ispit 
    ON studenti.br_ind = ispit.br_ind AND studenti.jmbg = ispit.jmbg
    INNER JOIN predmet
    ON ispit.predmet_id = predmet.predmet_id
    INNER JOIN rokovi
    ON ispit.rok = rokovi.rok_id
    ORDER BY rokovi.rok_naziv;";
    try{
        $prijave = $conn -> query($sql);
        $fetch = $prijave->fetchALL(PDO::FETCH_OBJ);
      }catch(PDOexception $e){
          echo $e -> getMessage();
          die();
      }
?>

  <table>
      <tr>
        <th>Ime</th>
        <th>Prezime</th>
        <th>Broj indeksa</th>
        <th>JMBG</th>
        <th>smer</th>
        <th>Predmet</th>
        <th>Ispitni rok</th>
      </tr>
        <?php
          foreach ($fetch as $f) {
        ?>
          <tr>
            <td>
              <?php echo $f->ime;?>
            </td>
            <td>
              <?php echo $f->prezime;?>
            </td>
            <td>
              <?php echo $f->br_ind;?>
            </td>
            <td>
              <?php echo $f->jmbg;?>
            </td>
            <td>
              <?php echo $f->smer;?>
            </td>
            <td>
              <?php echo $f->naziv_predmeta;?>
            </td>
            <td>
              <?php echo $f->rok_naziv;?>
            </td>
          </tr>
        <?php
          }

if($name <> 'admin') {header("Location: index.php");}
 include_once('footer.php');